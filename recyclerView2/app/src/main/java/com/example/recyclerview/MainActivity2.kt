package com.example.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
    }
    fun clickSave()
    {
        if(findViewById<TextView>(R.id.Name).text.toString().isEmpty() ||
            findViewById<TextView>(R.id.etLastName).text.toString().isEmpty() ||
            findViewById<TextView>(R.id.Email).text.toString().isEmpty() )
            Toast.makeText(this, "Please enter everything", Toast.LENGTH_SHORT).show()
        else
        {
            var user=User(findViewById<TextView>(R.id.Name).text.toString(),
                          findViewById<TextView>(R.id.etLastName).text.toString(),
                          findViewById<TextView>(R.id.Email).text.toString())
            val intent= intent
            intent.putExtra("INFO",user)
            setResult(RESULT_OK, intent)
            finish()
        }
    }
}