package com.example.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Adapter
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.databinding.MobileNumbersBinding

class personAdapter (var personList: MutableList<person>) :  RecyclerView.Adapter<personAdapter.ViewHolder>(){
   inner class ViewHolder(var binding: MobileNumbersBinding) :RecyclerView.ViewHolder(binding.root) {

    }
    public fun notifyItemChanged(position: Int, person:person)
    {

        notifyItemChanged(position,person)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    =ViewHolder(MobileNumbersBinding.inflate(LayoutInflater.from(parent.context),parent,false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.binding.tvName.text=personList[position].name
        holder.binding.tvLastName.text=personList[position].lastNAme
        holder.binding.tvEmail.text=personList[position].email
        holder.binding.btnDel.setOnClickListener {
            personList.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    override fun getItemCount(): Int = personList.size

}

