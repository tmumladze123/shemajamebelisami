package com.example.recyclerview

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    var personList= mutableListOf<person>(
        person("tamari " ,"Mumladze", "user@gmail.com"),
        person("tamari " ,"Mumladze", "user@gmail.com"),
        person("tamari " ,"Mumladze", "user@gmail.com"),
        person("tamari " ,"Mumladze", "user@gmail.com"),
        person("tamari " ,"Mumladze", "user@gmail.com"),
        person("tamari " ,"Mumladze", "user@gmail.com"),
        person("tamari " ,"Mumladze", "user@gmail.com"),
        person("tamari " ,"Mumladze", "user@gmail.com"),
        person("tamari " ,"Mumladze", "user@gmail.com"),
        person("tamari " ,"Mumladze", "user@gmail.com"),

    )

    private  lateinit var binding:ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        var adapter=personAdapter(personList)
        binding.rvNoteBook.adapter=adapter
        binding.rvNoteBook.layoutManager=LinearLayoutManager(this)
    }
    fun clickOnDelete()
    {

    }
    fun clickSave()
    {
        val intent= Intent(this, MainActivity2::class.java)
        getData.launch(intent)
    }
    val getData=registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if(it.resultCode == RESULT_OK) {
            var user: User? =it.data!!.getParcelableExtra<User>("INFO")
            user?.let { setInfo(it) }
        }
    }
    private fun setInfo(user: User) {
    }

}